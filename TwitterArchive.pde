/*
Twitter Archive Visual Maker
brendandawes.com

Make sure the Twitter tweets.csv is in .zip format.

*/

import java.text.SimpleDateFormat;
import java.io.*;
import java.util.regex.*;
import java.util.zip.*;
import java.util.*;
import java.text.*;
import javax.swing.*; 

import toxi.color.*; 
import toxi.util.datatypes.*;

final int BORDER = 300;
final int FONT_SIZE = 24;

ColorList palette = new ColorList();

BufferedReader reader;

SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

Date endDate;
Date startDate;

long startEpoch;
long endEpoch;

PFont font;

int counter = 0;

void setup(){
	size(A1w,A1h);
	font = loadFont("Futura-Medium-48.vlw"); 
  textFont(font);
  textSize(FONT_SIZE);
	background(255);
	noStroke();
	smooth();
	palette = getColorPalette();
	selectDataFile();
}

void draw(){
	try {
	String line = reader.readLine();
      if (line != null) {
        String[] pieces = splitLine(line);
        
        if (pieces.length > 7) {
        	try {
        		Date date = format.parse(pieces[5]);
        		if (startDate == null) {
        			startDate = date;
        			startEpoch = startDate.getTime()/1000;
        		}
        		counter++;
        		println("counter: "+counter);
        		String text = pieces[7];
  				  int numberOfMinutes = getNumberOfMinutes(date);
  				  float mappedX = map(numberOfMinutes,0,1440,BORDER,width-BORDER);
  				  float snappedXpos = floor( mappedX / FONT_SIZE ) * FONT_SIZE ;
        		long epoch = date.getTime()/1000;
        		float mappedY = map(epoch, startEpoch, endEpoch, BORDER, height-BORDER);
        		color c = palette.get(text.length()).toARGB();
        		fill(c);
        		float r = noise(snappedXpos,mappedY);
        		if ( r < 0.6) {
        				drawVerticalBar(snappedXpos,mappedY,text.length());
        		} else {
        			if (doesNotClash(snappedXpos,mappedY) && textWillFitOnPage(mappedY,date.toString()+" "+text)) {
        				drawText(date.toString()+" "+text,snappedXpos,mappedY);
        			} else {
        				drawVerticalBar(snappedXpos,mappedY,text.length());
        			}
        		}

        	} catch(ParseException pe) {
            
          }
    	}
    } else {

    	saveImage();
    	exit();
    }
} catch (IOException e) {

      e.printStackTrace();
    }
		
}

void saveImage() {

	saveFrame("TwitterArchive-####.tif");

}

void drawText(String txt,float x, float y){

	translate(x,y);
	pushMatrix();
	rotate(radians(90));
	text(txt, 0,0); 
	popMatrix();

}

void drawVerticalBar(float x, float y, int len) {

	rect(x,y,3,len);
}



Boolean textWillFitOnPage(float y, String txt) {

  if ((y + textWidth(txt)) > (height-BORDER)) {
    return false;
  } else {
    return true;
  }

}

Boolean doesNotClash(float x, float y) {

	loadPixels();
	for (int i=int(y); i < y+50; i++) {
		for (int j=int(x); j < x+12; j++) {
		int loc = i*width+j;
		float r = red(pixels[loc]);
      	float g = green(pixels[loc]);
      	float b = blue(pixels[loc]);
      	if (r != 255 || g != 255 || b != 255) {
      			return false;	
      		}
  		}
	}
	return true;
}

int getNumberOfMinutes(Date date) {

	SimpleDateFormat ft = new SimpleDateFormat ("H");
  	int hours =  int(ft.format(date));
  	ft = new SimpleDateFormat ("m");
  	int minutes = int(ft.format(date));
  	return (hours*60)+minutes;
}

ColorList getColorPalette() {
   
   ColorTheme theme = new ColorTheme("theme");
   theme.addRange("warm teal",0.5);
   theme.addRange("warm ivory",0.5);
   theme.addRange("warm purple",0.5);
   theme.addRange("warm red",0.5);
   theme.addRange("warm yellow",0.5);
   ColorList l = theme.getColors(255).sort();
   return l;
   
   
 }

void selectDataFile() {
  try { 
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
  } 
  catch (Exception e) { 
    e.printStackTrace();
  }  
 
  final JFileChooser fc = new JFileChooser(); 

  int returnVal = fc.showOpenDialog(this); 
  if (returnVal == JFileChooser.APPROVE_OPTION) { 
    File file = fc.getSelectedFile(); 
    String path = file.getPath();
    readZipFile(path);
  } else {
    exit();
  }
}

void readZipFile(String filename) {

  try {

    String path = dataPath(filename);
    ZipFile file = new ZipFile(path);
    Enumeration entries = file.entries();
    while (entries.hasMoreElements ()) {
      ZipEntry entry = (ZipEntry) entries.nextElement();
      String name = entry.getName();
      String outputPath = savePath(name);
      InputStream stream = file.getInputStream(entry);
      setEndDateUsingLastLineOfCSV(stream);
      stream = file.getInputStream(entry);
      reader = createReader(stream);
      break;
    }
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
}

void setEndDateUsingLastLineOfCSV(InputStream stream) {

  String lines[] = loadStrings(stream);
  println(lines[lines.length-1]);
  String[] pieces = splitLine(lines[lines.length-1]);
  try {
    endDate = format.parse(pieces[5]);
    endEpoch = endDate.getTime()/1000;
  } catch (ParseException pe) {
    println("Couldn't parse end date");
    exit();
  }
  

}

 void mousePressed() {

 	saveImage();

 	}

// Ben Fry's CSV parser from the book Visualising Data

String[] splitLine(String line) {


  char[] c= line.toCharArray();
  ArrayList pieces = new ArrayList();
  int prev = 0;
  boolean insideQuote = false;
  for (int i=0; i < c.length; i++) {
    if (c[i] == ',') {
      if (!insideQuote) {
        String s = new String(c, prev, i-prev).trim();
        pieces.add(s);
        prev = i+1;
      }
    } 
    else if (c[i] == '\"') {
      insideQuote = !insideQuote;
    }
  }
  if (prev != c.length) {
    String s = new String(c, prev, c.length - prev).trim();
    pieces.add(s);
  }
  String[] outgoing = new String[pieces.size()];
  pieces.toArray(outgoing);
  scrubQuotes(outgoing);
  return outgoing;
}

void scrubQuotes(String[] array) {

  for (int i=0; i < array.length; i++) {
    if (array[i].length() > 2) {
      if (array[i].startsWith("\"") && array[i].endsWith("\"")) {
        array[i] = array[i].substring(1, array[i].length()-1);
      }
    }
    array[i] = array[i].replaceAll("\"\"", "\"");
  }
}