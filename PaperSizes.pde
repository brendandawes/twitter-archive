/* 
ISO 216 paper sizes converted to pixels @ 300dpi

Drop this file in your Processing sketch and use like so: size(A1w, A1h);
*/

final int A5w = 1748;
final int A5h = 2480;

final int A4w = 2480;
final int A4h = 3508;

final int A3w = 3508;
final int A3h = 4961;

final int A2w = 4961;
final int A2h = 7016;

final int A1w = 7016;
final int A1h = 9933;

final int A0w = 9933;
final int A0h = 14043;